package SW.data.crawler;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** 
* CopyRright(c)2016-2-26:<HD>                          
* Project:<视问-项目 >  
* Module:爬虫                                                                                
* JDK version used:<JDK1.7>                                                            
* Author:<Gang Hu>                 
* Create Date: <创建日期:2016-2-26>                                         
* Version:0.1
* Comments:主函数入口，运行整个系统
*/ 

public class Run {
	 public static void main(String args[]) throws Exception {
			String homeUrl = "http://www.baike.com/wiki/";
			String seedBase="G:/DataCollect/Seed/Seed.txt";
			String seedPath="G:/DataCollect/Seed/Seed20160307.txt";
			String filePathPrexi="G:/DataCollect/";
			String searchWord=null;  
			String runUrl =null;//homeUrl+ keyWords;
			ArrayList<String> seed=null;
			 
			System.out.println("Begin");
			FileOpInterfaceImp fp = new FileOpInterfaceImp(seedPath);
			fp.checkCopy(seedPath, seedBase);
			seed = fp.readTxtByLine(fp.filePath);
			
			for(int i=0;i<seed.size();i++){
				searchWord = seed.get(i);
				runUrl = homeUrl+searchWord;
				CrawlerInterfaceImp crawler = new CrawlerInterfaceImp();
				String htmlContent = crawler.getByString(runUrl);//get 
				ContentFilter cf = new ContentFilter(htmlContent);
				
				if(cf.judeErro()){
					System.out.println("未收录");
				}else{
				String oriPath=filePathPrexi+"OriginalWeb/"+searchWord+".html";
				String purePath=filePathPrexi+"PureData/"+searchWord+".txt";
				fp = new FileOpInterfaceImp(oriPath);
				fp.writeFile(fp.filePath, htmlContent);
				fp = new FileOpInterfaceImp(purePath);
				fp.writeFile(fp.filePath, cf.getMainSection());
				
				System.out.println(oriPath);
				Metadata rowRe = new Metadata(searchWord,cf.getDescription(),cf.getKeywords(),oriPath,purePath,runUrl);
				DatabaseOpInterfaceImp dp = new DatabaseOpInterfaceImp(); 
				dp.insert(rowRe);
			}
			System.out.println("End");
	    }
			}
}
