package SW.data.crawler;

import java.util.ArrayList;

/** 
* CopyRright(c)2016-2-26:<HD>                          
* Project:<视问-项目 >
* Module:爬虫                                                                                  
* JDK version used:<JDK1.7>                                                            
* Author:<Gang Hu>                 
* Create Date: <创建日期:2016-2-26>                                         
* Version:0.1
* Comments:文件操作接口。
*/ 

public interface FileOpInterface {
	static final int WRITE_SUCCESS = 1;
	static final int WRITE_FAIL = 0;
	static final int CREATE_SUCCESS = 1;
	static final int CREATE_FAIL = 0;  
	static final int CLEAR_SUCCESS = 0; 
	static final int CLEAR_FAIL = 0; 
	
	/**
	 * write txt  to file
	 * 
	 *  @param filePath
	 * 				the path of txt.
	 *			content
	 *				the content of txt.
	 *  
	 *  @return if write success or not.
	 * 				
	 */
	public int writeFile(String path,String content);
	
	/**
	 * read txt  by line
	 * 
	 *  @param filePath
	 * 				the path of txt.
	 *  
	 *  @return a ArrayList saved content.
	 * 				
	 */
	public ArrayList<String> readTxtByLine(String filePath);
}
