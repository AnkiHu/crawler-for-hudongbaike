package SW.data.crawler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

/** 
* CopyRright(c)2016-2-26:<HD>                          
* Project:<视问-项目 >
* Module:爬虫                                                                                  
* JDK version used:<JDK1.7>                                                            
* Author:<Gang Hu>                 
* Create Date: <创建日期:2016-2-26>                                         
* Version:0.1
* Comments:文件操作实现。
*/ 

public class FileOpInterfaceImp implements FileOpInterface{
	String filePath = null;
	public FileOpInterfaceImp(String filePath){
		this.filePath = filePath;
	}
	
	public int writeFile(String path,String content){
		File saveFile = new File(path);
		
		if (!saveFile.exists()) {
			try {
				saveFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try {
			BufferedWriter TxtWriter =new BufferedWriter(new FileWriter(filePath));
			TxtWriter.write(content);
			TxtWriter.flush();
			TxtWriter.close();
			TxtWriter=null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("IO Exception");
		}
		saveFile=null;
		return WRITE_SUCCESS;
	}
	
	public ArrayList<String> readTxtByLine(String filePath) {
		// TODO Auto-generated method stub
		File rTxtFile = new File(filePath);
		BufferedReader Txtreader =null;
		String lineContent=null;
		ArrayList<String> txtContent = new ArrayList<String>();
		
		if(!rTxtFile.exists()){
			System.out.println(filePath+"does not exist!");
			return null;
		}
		else{
			try {
				Txtreader = new BufferedReader(new FileReader(filePath));
				while((lineContent = Txtreader.readLine())!=null){
					txtContent.add(lineContent);
				}
				Txtreader.close();
				Txtreader=null;
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.err.println("File not found");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.err.println("IO Exception");
			}
			rTxtFile=null;
			return txtContent;
		}
	}
	
	public int writeTxt(String filePath, ArrayList<String> content) {
		// TODO Auto-generated method stub
		File wTxtFile = new File(filePath);
		BufferedWriter TxtWriter =null;
		if(!wTxtFile.exists()){
			System.out.println(filePath+"does not exist!");
			return WRITE_FAIL;
		}else{
			try {
				TxtWriter = new BufferedWriter(new FileWriter(filePath));
				for(int i=0;i<content.size();i++){
					TxtWriter.write(content.get(i)+"\n");
				}
				TxtWriter.flush();
				TxtWriter.close();
				TxtWriter=null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.err.println("IO Exception");
			}
			wTxtFile=null;
			return WRITE_SUCCESS;
		}
	}

	public ArrayList<String> checkCopy(String newFile,String oriFile) {
		// TODO Auto-generated method stub
		ArrayList<String>  oriWord = readTxtByLine(oriFile); 
		ArrayList<String>  newWord = readTxtByLine(newFile); 
		
		for(int i=0;i<newWord.size();i++){
			if(oriWord.contains(newWord.get(i))){
				newWord.remove(i);
				i--;
			}
			else{
				oriWord.add(newWord.get(i));
			}
		}
		
		for(int i=0;i<newWord.size();i++){
			System.out.println(newWord.get(i));
		}
		writeTxt(oriFile,oriWord);
		writeTxt(newFile,newWord);
		newWord=null;
		oriWord=null;
		return newWord;
		
	}

}
