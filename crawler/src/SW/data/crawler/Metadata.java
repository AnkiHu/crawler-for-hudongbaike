package SW.data.crawler;

/** 
* CopyRright(c)2016-2-26:<HD>                          
* Project:<视问-项目 >
* Module:爬虫                                                                                  
* JDK version used:<JDK1.7>                                                            
* Author:<Gang Hu>                 
* Create Date: <创建日期:2016-2-26>                                         
* Version:0.1
* Comments:封装爬取结果，以备存入数据库时使用。
*/ 

public class Metadata {
	/*
	 * this class to save the insert data
	 * 
	 * */
	private String searchWord;
	private String descripition;
	private String keyWords;
	private String oriPath;
	private String purePath;
	private String url;
	
	public Metadata(String searchWord,String descripition,String keyWords,String oriPath,String purePath,String url){
		this.searchWord = searchWord;
		this.descripition = descripition;
		this.keyWords = keyWords;
		this.oriPath = oriPath;
		this.purePath = purePath;
		this.url = url;
	}

	public String getSearchWord() {
		return searchWord;
	}

	public void setSearchWord(String searchWord) {
		this.searchWord = searchWord;
	}

	public String getDescripition() {
		return descripition;
	}

	public void setDescripition(String descripition) {
		this.descripition = descripition;
	}

	public String getKeyWords() {
		return keyWords;
	}

	public void setKeyWords(String keyWords) {
		this.keyWords = keyWords;
	}

	public String getOriPath() {
		return oriPath;
	}

	public void setOriPath(String oriPath) {
		this.oriPath = oriPath;
	}

	public String getPurePath() {
		return purePath;
	}

	public void setPurePath(String purePath) {
		this.purePath = purePath;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	
	
}
