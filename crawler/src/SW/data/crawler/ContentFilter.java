package SW.data.crawler;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;

public class ContentFilter {

	String html = null;
	int begin;
	int end;
	String res = null;

	public ContentFilter(String html) {
		this.html = html;
	}

	public String getDescription() {
		begin = html.indexOf("<meta content=\"");
		end = html.indexOf("\" name=\"description\">");
		res = html.substring(begin, end);
		res = res.replaceAll("<meta content=\"", "");
		return res;
	}

	public String getKeywords() {
		begin = html.indexOf("<meta name=\"keywords\" content=\"");
		end = html.indexOf("\">", begin);
		res = html.substring(begin, end);
		res = res.replaceAll("<meta name=\"keywords\" content=\"", "");
		return res;
	}
	
	public  boolean judeErro() {
		Pattern pattern = Pattern.compile("\\?\\?\\?\\?+",Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(html);
		return matcher.find();
	}
	
	public String getMainSection() {
		
		begin = html.indexOf("<!--google_ad_section_start-->");
		end = html.indexOf("<!--google_ad_section_end-->", begin);
		res = html.substring(begin, end);
		System.out.println();
		res = res.replaceAll("<!--google_ad_section_start-->", "");
		
		 String regEx_script="<script[^>]*?>[\\s\\S]*?<\\/script>"; //定义script的正则表达式 
	        String regEx_style="<style[^>]*?>[\\s\\S]*?<\\/style>"; //定义style的正则表达式 
	        String regEx_html="<[^>]+>"; //定义HTML标签的正则表达式 
	         
	        Pattern p_script=Pattern.compile(regEx_script,Pattern.CASE_INSENSITIVE); 
	        Matcher m_script=p_script.matcher(res); 
	        res=m_script.replaceAll(""); //过滤script标签 
	         
	        Pattern p_style=Pattern.compile(regEx_style,Pattern.CASE_INSENSITIVE); 
	        Matcher m_style=p_style.matcher(res); 
	        res=m_style.replaceAll(""); //过滤style标签 
	         
	        Pattern p_html=Pattern.compile(regEx_html,Pattern.CASE_INSENSITIVE); 
	        Matcher m_html=p_html.matcher(res); 
	        res=m_html.replaceAll(""); //过滤html标签 


		return res;
	}

}
