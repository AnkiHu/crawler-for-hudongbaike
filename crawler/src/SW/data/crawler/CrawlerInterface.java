package SW.data.crawler;

import java.sql.Connection;

/** 
* CopyRright(c)2016-2-26:<HD>                          
* Project:<视问-项目 >
* Module:爬虫                                                                                  
* JDK version used:<JDK1.7>                                                            
* Author:<Gang Hu>                 
* Create Date: <创建日期:2016-2-26>                                         
* Version:0.1
* Comments:根据指定的链接入口和关键词抓取网页接口。
*/ 

public interface CrawlerInterface {
	/**
	 * get html from web
	 * 
	 *  @param url
	 * 				the url to submit
	 *  
	 *  @return content of html.
	 * 				
	 */
	public String getByString(String url)throws Exception;
}
