package SW.data.crawler;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/** 
* CopyRright(c)2016-2-26:<HD>                          
* Project:<视问-项目 >
* Module:爬虫                                                                                  
* JDK version used:<JDK1.7>                                                            
* Author:<Gang Hu>                 
* Create Date: <创建日期:2016-2-26>                                         
* Version:0.1
* Comments:根据指定的链接入口和关键词抓取网页实现。
*/ 

public class CrawlerInterfaceImp implements CrawlerInterface{
	 
    public String getByString(String url) throws Exception {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(100000).setConnectTimeout(100000).build(); 

        String responseBody =null;
        String response =null;
        try {
            HttpGet httpget = new HttpGet(url);
            httpget.setConfig(requestConfig);  
            //System.out.println("executing request " + httpget.getURI());
 
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
 
                public String handleResponse(
                        final HttpResponse response) throws ClientProtocolException, IOException {
             
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }
            };
            responseBody = httpclient.execute(httpget, responseHandler);
           
            try { 
                byte[] temp=responseBody.getBytes("ISO-8859-1");//这里写原编码方式  
                response=new String(temp,"utf-8");//这里写转换后的编码方式 
              } catch (UnsupportedEncodingException e) { 
                  // TODO Auto-generated catch block 
                  e.printStackTrace(); 
              }
        } finally {
            httpclient.close();
        }
        return response;
    }
    
    public static String getEncoding(String str) { 
    	 String encode = "GB2312"; 
    	 try { 
    	 if (str.equals(new String(str.getBytes(encode), encode))) { //判断是不是GB2312
    	 String s = encode; 
    	 return s; //是的话，返回“GB2312“，以下代码同理
    	 } 
    	 } catch (Exception exception) { 
    	 } 
    	 encode = "ISO-8859-1"; 
    	 try { 
    	 if (str.equals(new String(str.getBytes(encode), encode))) { //判断是不是ISO-8859-1
    	 String s1 = encode; 
    	 return s1; 
    	 } 
    	 } catch (Exception exception1) { 
    	 } 
    	 encode = "UTF-8"; 
    	 try { 
    	 if (str.equals(new String(str.getBytes(encode), encode))) { //判断是不是UTF-8
    	 String s2 = encode; 
    	 return s2; 
    	 } 
    	 } catch (Exception exception2) { 
    	 } 
    	 encode = "GBK"; 
    	 try { 
    	 if (str.equals(new String(str.getBytes(encode), encode))) { //判断是不是GBK
    	 String s3 = encode; 
    	 return s3; 
    	 } 
    	 } catch (Exception exception3) { 
    	 } 
    	 return ""; //如果都不是，说明输入的内容不属于常见的编码格式。
    	 } 
}
