package SW.data.crawler;
/** 
* CopyRright(c)2016-2-26:<HD>                          
* Project:<视问-项目 >
* Module:爬虫                                                                                  
* JDK version used:<JDK1.7>                                                            
* Author:<Gang Hu>                 
* Create Date: <创建日期:2016-2-26>                                         
* Version:0.1
* Comments:数据库操作接口。
*/ 

public interface DatabaseOpInterface {
	static final int INSERT_SUCCESS = 1;
	/**
	 *insert into database
	 *
	 * @param row 
	 * 			the row to insert into database.
	 * 
	 * @return a flag to present whether insert success.
	 * 
	 */
	public int insert(Metadata row);
}
